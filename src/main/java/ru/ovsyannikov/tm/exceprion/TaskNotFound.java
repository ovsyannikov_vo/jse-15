package ru.ovsyannikov.tm.exceprion;

public class TaskNotFound extends Exception{
    public TaskNotFound(String message) {
        super(message);
    }

}
