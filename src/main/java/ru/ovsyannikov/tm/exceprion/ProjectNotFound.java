package ru.ovsyannikov.tm.exceprion;

public class ProjectNotFound extends Exception {
    public ProjectNotFound(String message) {
        super(message);
    }

}
